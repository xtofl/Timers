# Timers app

This is an app that allows Parents to grant 'blocks' of time
to their kids (Jona) to use (for e.g. screen time).

Kids can use their blocks; parents can check the amount of
time left.


# Use cases

## Jona wants to play

* Jona opens HIS Timer app
* Timer shows selection: 2 blocks? 
* Jona taps the 2 blocks button: [jona/start](api/start.php?timer=jona)
* Timer app starts; shows Stop button
* Timer shows an update of the time left (regularly) [jona/timeleft](api/timeleft.php?timer=jona)
* Timer warns Jona when only 5' left
* Jona stops timer

* When time goes negative, parent is notified

## Xtofl grant time

* Xtofl opens PARENT Timer app
* Xtofl creates timer "jona" [create/jona](api/create.php?timer=jona)
* Timer shows available Timers (named) [timers](api/timers.php)
* Xtofl can change the Timer interval (15')
* Xtofl can change amount of time left (6 blocks)
* Timer shows amount of blocks left (and equivalent time)

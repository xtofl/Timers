<?php
$arguments = &$_GET;
$timer = $arguments['timer'];

$root = dirname(dirname(dirname(__FILE__)));
$timer_file = "$root/timers/$timer";

$f = fopen($timer_file, 'w') or die('file exists');

$timer = [
	"name" => $timer,
	"events"=> [
		["type"=>"created",
		"timestamp"=>new DateTime("now")]
	]
];

fwrite($f, json_encode($timer));

?>

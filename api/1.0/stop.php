<?php
$arguments = &$_GET;
$timer = $arguments['timer'];

$root = dirname(dirname(dirname(__FILE__)));
$timer_file = "$root/timers/$timer";

$values = json_decode(file_get_contents($timer_file));
print_r($values);

$now = new DateTime("now");
$values->events[]=["type"=>"stop", "timestamp"=>$now];
file_put_contents($timer_file, json_encode($values));

?>

<?php
header('Content-Type: application/json');
require("../../app.inc.php");

$timer = read_timer();
$granted=total_granted_today($timer);
$revoked=total_revoked_today($timer);
echo json_encode([
	"spent"=>total_spent_today($timer)->format("%H:%I:%S"),
	"granted"=>$granted->format("%H:%I:%S"),
	"revoked"=>$revoked->format("%H:%I:%S"),
	"granted_net"=>time_diff($granted, $revoked)->format("%H:%I:%S"),
	"ratio"=>spent_ratio($timer)
]);

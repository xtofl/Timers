<?php
header('Content-Type: application/json');
require("../../app.inc.php");

$timer = read_timer();
$day = array_key_exists("day", $_GET) ? $_GET["day"] : "now";
#$day = $_GET["day"] ?? "now";
echo json_encode(intervals_today($timer, $day), JSON_PRETTY_PRINT);

<?php

function timer_file() {
	$arguments = &$_GET;
	$timer = $arguments['timer'];

	$root = dirname(__FILE__);
	return "$root/timers/$timer";
}


function read_timer($file=null) {
	$root = dirname(__FILE__);
	$values = json_decode(file_get_contents(is_null($file) ? timer_file(): "$root/timers/$file"));
	return $values;
}

function timer_files() {
	$root = dirname(__FILE__);
	return array_map(basename, glob("$root/timers/*"));
}

function write_timer($timer) {
	file_put_contents(timer_file(), json_encode($timer));
}

function event_timestamp($event) {
	return new DateTime($event->timestamp->date);
}

function same_day($t1, $t2) {
	return $t1->format('Y-m-d') == $t2->format('Y-m-d');
}
function events_today_filtered($timer, $type, $day) {
	$today = $day ? $day : new DateTime("now");
	return array_filter($timer->events, function($e) use ($today, $type){
		$t = event_timestamp($e);
		return $e->type == $type and same_day($t, $today);
	});
}
function revoked_today($timer) {
	$today = new DateTime("now");
	return events_today_filtered($timer, "revoke", $today);
}
function revoked_amount($r){
	$amount = $r->amount;
	return new DateInterval("PT15M");//".$amount["i"]."M");
}

function total_revoked_today($timer){
	$amounts = array_map(
		revoked_amount,
		revoked_today($timer));

	$zero = new DateTimeImmutable();
	return $zero->diff(array_reduce($amounts,
		function($sum, $amount){
			return $sum->add($amount);
		},
		$zero));
}

function time_diff($t1, $t2){
	$now = new DateTimeImmutable("now");
	return $now->add($t1)->diff($now->add($t2));
}
function default_granted($today){
	$weekday = $today->format('w');
	$amount = [
		1=>2,
		2=>2,
		3=>3,
		4=>2,
		5=>2,
		6=>6,
		7=>6
	][$weekday];

	$events = [];
	for($i=0; $i!=$amount; $i++){
		$events[]= [
			"type"=>"grant",
			"note"=>"default on day $weekday",
			"timestamp"=>$today,
			"amount"=>new DateInterval("PT15M")];
	}
	return $events;
}
function granted_today($timer) {
	$today = new DateTime("now");
	return array_merge(default_granted($today), events_today_filtered($timer, "grant", $today));
}
function granted_amount($granted){
	$amount = $g->amount;
	return new DateInterval("PT15M");//".$amount["i"]."M");
}

function total_granted_today($timer){
	$granted = array_map(
		granted_amount,
		granted_today($timer));

	$zero = new DateTimeImmutable();
	return $zero->diff(array_reduce($granted,
		function($sum, $amount){
			return $sum->add($amount);
		},
		$zero));
}

function events_today($timer, $day = "now") {
	$today = new DateTime($day);
	return array_filter($timer->events, function($e) use ($today){
		$t = event_timestamp($e);
		return $t->format('Y-m-d') == $today->format('Y-m-d');
	});
}

function intervals_today($timer, $day = "now") {
	$result = [];
	$await = ""; $action = null;
	$start_interval = null;
	$stop_interval = null;
	$stop_interval =  function($when) use (&$result, &$await, &$action, &$start_interval){
		end($result);
		$result[key($result)]["stop"] = $when;
		$result[key($result)]["busy"] = false;
		$await = "start"; $action = $start_interval;
	};

	$start_interval = function($when) use (&$result, &$await, &$action, &$stop_interval){
		$result[]=["start"=>$when];
		$await = "stop"; $action = $stop_interval;
	};
	$await = "start"; $action = $start_interval;
	foreach(events_today($timer, $day) as $e) {
		if($e->type == $await) {$action(event_timestamp($e));}
	}
	if ($await == "stop") {
		$stop_interval(new DateTime("now"));
		$result[key($result)]["busy"] = true;
	}
	return $result;
}

function total_spent_today($timer) {
	$intervals = intervals_today($timer);
	$zero = new DateTimeImmutable("00:00");
	return $zero->diff(array_reduce($intervals, function($sum, $interval) use($zero) {
			$diff = $interval["start"]->diff($interval["stop"]);
			return $sum->add($diff);
		}, $zero));
}

function open_interval($timer) {
	$last_event = end($timer->events);
	if($last_event->type == "stop"){
		return null;
	} else {
		$stop = new DateTime("now");
		$start = event_timestamp(prev($timer->events));
		$diff = $start->diff($stop);
		return ["start" => $start,
			"stop" => $stop,
			"diff"=> $diff
		];
	}
}

function datetimediff_seconds($diff){
	$now = new DateTimeImmutable();
	$then = $now->add($diff);
	return $then->getTimestamp() - $now->getTimestamp();
}

function spent_ratio($timer){
	$total = datetimediff_seconds(total_spent_today($timer));
	$granted = datetimediff_seconds(total_granted_today($timer));
	return $granted != 0 ? $total/$granted : -1;
}

?>

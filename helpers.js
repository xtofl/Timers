
var aQuarter = new Date('1970-01-01T00:00:15Z');
var epsilon = 1e-9;
function findFirstAfter(t, increment){
	var time = t.getTime();
	var diff = increment.getTime();
	var rest = time % diff;
	var nextTime = (time - rest) + diff;
	return new Date(nextTime);
}

Date.prototype.between = function(t1, t2) {
	return (t1.getTime() < this.getTime() && this.getTime() <= t2.getTime());
}
Date.prototype.add = function(diff){
	this.setTime( this.getTime() + diff.getTime() );
}

function monitor(timer){
	var previousDate = null;
	var nextAlarm = null;
	return start_refresh_loop(timer, function(json){
		var t = new Date('1970-01-01T' + json.spent + 'Z');
		if (!nextAlarm) {
			nextAlarm = findFirstAfter(t, aQuarter);
		}
		if (previousDate && nextAlarm.between(previousDate, t)) {
			if (navigator.vibrate) { navigator.vibrate([200, 100, 200, 1000, 200,100,1000]); }
			nextAlarm = findFirstAfter(nextAlarm, aQuarter);
		}
		previousDate = t;
		report_status(previousDate);
	});
}

<?php
require("app.inc.php");
?>
<html>
<head>
<style>
table { border: 1px solid black; }
.button { border: 2px solid black; background-color:lightgrey; }
.good { background-color: lightgreen; }
</style>
<script src="timer.js?v=<?php echo md5_file('timer.js'); ?>"></script>
</head>
<body>
<table>
<?php
foreach(array_map(read_timer, timer_files()) as $timer){
	$row = "<tr><td><a href='/timer:$timer->name'>$timer->name</a></td>"
		."<td><button onclick='start(\"$timer->name\")'>start</button></td>"
		."<td id='$timer->name' onclick='refresh(\"$timer->name\")'>(running)</td>"
		."<td><progress id='$timer->name"."_ratio'></progress></td>"
		."<td><button onclick='stop(\"$timer->name\")'>stop</button></td>"
	;
	if(array_key_exists("parent", $_GET)){
		$row .= "<td class='button'><a onclick='grant(\"$timer->name\")'>grant extra</a></td>";
		$row .= "<td class='button'><a onclick='revoke(\"$timer->name\")'>revoke extra</a></td>";
	 }
	$row .= "</tr>";
	print($row);
	print("<script>start_refresh_loop(\"$timer->name\");</script>");
}
if(array_key_exists("parent", $_GET)){
?>
<tr><td>create... timer<input id="create_name" type="text" placeholder="name"/><td><td><input type="button" onclick="foo()" value="create"></input></td></tr>
<?php
}
?>
</table>
<div id='status'></div>
</body>
</html>

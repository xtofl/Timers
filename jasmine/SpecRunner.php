<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Jasmine Spec Runner v2.8.0</title>

  <link rel="shortcut icon" type="image/png" href="lib/jasmine-2.8.0/jasmine_favicon.png">
  <link rel="stylesheet" href="lib/jasmine-2.8.0/jasmine.css">

  <script src="lib/jasmine-2.8.0/jasmine.js"></script>
  <script src="lib/jasmine-2.8.0/jasmine-html.js"></script>
  <script src="lib/jasmine-2.8.0/boot.js"></script>

  <!-- include source files here... -->
  <script src="/helpers.js?v=<?php echo md5_file('../helpers.js'); ?>"></script>
  <script src="/timer.js?v=<?php echo md5_file('../timer.js'); ?>"></script>

  <!-- include spec files here... -->
  <script src="/jasmine/spec/SpecHelper.js?v=<?php echo md5_file('helper.js'); ?>"></script>
  <script src="/jasmine/spec/timerSpec.js?v=<?php echo md5_file('../jasmine/spec/timerSpec.js'); ?>"></script>

</head>

<body>
</body>
</html>

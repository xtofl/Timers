describe("timer_helper", function() {
  var timer;

  beforeEach(function() {
  });
  var t0 = new Date("1970-01-01T00:00:00Z");
  var t1 = new Date("1970-01-01T00:00:10Z");
  var t2 = new Date("1970-01-01T00:00:20Z");
  var increment = new Date("1970-01-01T00:00:10Z");

  // demonstrates use of spies to intercept and test method calls
  it("should find the next quarter after t0", function() {

	var firstAlarmTime = findFirstAfter(t0, increment);
	expect(firstAlarmTime).toEqual(new Date("1970-01-01T00:00:10Z"));
	var nextAlarmTime = findFirstAfter(firstAlarmTime, increment);
    expect(nextAlarmTime).toEqual(new Date("1970-01-01T00:00:20Z"));
  });
  it("should define a 'between' function for Date", function () {
	expect(t1.between(t0, t1)).toBeTruthy();
	expect(t1.between(t1, t2)).toBeFalsy();
  });
});

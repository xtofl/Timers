function access(timer, api, callback){
	var r = new XMLHttpRequest();
	r.onreadystatechange = function(s){
		if(r.readyState === XMLHttpRequest.DONE) {
			callback && callback(r);
		}
	};
	r.open("GET", api, true);
	r.send();
	return r;
}
function report_status (e){
	var element = document.getElementById('status');
	if(element) element.innerText = e;
}
function delete_timer(timer){
	access(timer, "api/1.0/timer/"+timer+"/delete", report_status);
}
function start(timer){
	access(timer, "api/1.0/timer/"+timer+"/start", report_status);
}
function stop(timer){
	access(timer, "api/1.0/timer/"+timer+"/stop", report_status);
}
function grant(timer){
	access(timer, "api/1.0/timer/"+timer+"/grant", report_status);
}
function revoke(timer){
	access(timer, "api/1.0/timer/"+timer+"/revoke", report_status);
}
function update_elements(timer, json) {
	var e = document.getElementById(timer);
	e.innerText = json.spent + " / " + json.granted_net;
	if(json.revoked != "00:00:00") {
		e.innerText += " (-"+json.revoked+")";
	}
	var r = document.getElementById(timer+"_ratio");
	if(r){
		r.value = json.ratio;
	}
}
function refresh(timer, f){
	access(timer, "api/1.0/timer/"+timer+"/timeleft", function(r){
		var json = JSON.parse(r.response);
		update_elements(timer, json);
		if(f) f(json);
	});
}
function value(id){
	var e = document.getElementById(id);
	return e.value;
};
function create(timer) {
	access(timer, "api/1.0/create.php?timer="+timer, report_status);
	start_refresh_loop(timer);
}
function start_refresh_loop(timer, f){
	refresh(timer);
	return setInterval(function(){refresh(timer, f)}, 1000);
}
function with_intervals(timer, f){
	access(timer, "api/1.0/timer/"+timer+"/today", function(r){
		f(JSON.parse(r.response));
	});
}
function foo(){ create(value('create_name'));}

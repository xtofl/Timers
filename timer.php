<?php
require("app.inc.php");
$timer = read_timer();
?>
<html>
<head>
<style>
table { border: 1px solid black; }
body { width: 10cm; }
button { width: 100%; height: 2cm; }
progress { width: 100%; }
.good { background-color: lightgreen; }
</style>
<script src="timer.js?v=<?php echo md5_file('timer.js'); ?>"></script>
<script src="helpers.js?v=<?php echo md5_file('helpers.js'); ?>"></script>
</head>
<body>
<div>
<div><button onclick='start("<?php echo $timer->name; ?>")'>start</button></div>
<div class='good' id='<?php print($timer->name);?>' onclick='refresh("<?php print($timer->name);?>")'>(running)</div>
<progress min='0' max='1' id='<?php print($timer->name);?>_ratio'>?/?</progress>
<div><button onclick='stop("<?php echo $timer->name; ?>")'>stop</button></div>
<script>monitor("<?php echo $timer->name?>");</script>
<ul id="details"></ul>
<script>
function localTime(t){
	return t.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
}

with_intervals("<?=$timer->name?>", function(intervals){
	var e = document.getElementById("details");
	while(e.firstChild) e.removeChild(e.firstChild);

	intervals.forEach(function(i){
		var n = document.createElement('li');
		var start = new Date(i.start.date);
		var stop = new Date(i.stop.date);
		n.appendChild(document.createTextNode(localTime(start) + ' - ' + localTime(stop)));
		e.appendChild(n);
	});
})</script>
</div>
<a href="/">overview</a>
<div id="status"></div>
</body>
</html>
